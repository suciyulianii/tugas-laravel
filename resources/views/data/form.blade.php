<!DOCTYPE html>
<html>
    <head>
        <title> Sign Up Form</title>
        <meta charset="UTF-8">
    </head>
    <body>
        <!--Bagian judul utama-->
        <div>
            <h1>Buat Account Baru!</h1>
            <h3>Sign Up Form</h3>
        </div>
        <form action="/login" method="POST">
            <label for="nama_awal">First name:</label> <br><br>
            <input type="text" id="nama_awal">
            
            <br><br>
            
            <label for="nama_akhir">Last name:</label> <br><br>
            <input type="text" id="nama_akhir">
            
            <br><br>

            <label>Gender:</label> <br><br>
            <input type="radio" name="gender" value="L"> Male <br>
            <input type="radio" name="gender" value="P"> Female <br>
            <input type="radio" name="gender" value="O"> Other

            <br><br>

            <label>Nationality:</label><br><br>
            <select name="kota_domisili">
                <option value="Indonesian">Indonesian</option>
                <option value="Singaporean">Singaporean</option>
                <option value="Malaysian">Malaysian</option>
                <option value="Australian">Australian</option>
            </select>

            <br><br>

            <label>Language Spoken:</label> <br><br>
            <input type="checkbox" name="bahasa" value="Bahasa Indonesia"> Bahasa Indonesia <br>
            <input type="checkbox" name="bahasa" value="English"> English <br>
            <input type="checkbox" name="bahasa" value="Other"> Other <br>

            <br>

            <label for="bio">Bio:</label> <br><br>
            <textarea cols="30" rows="10" id="bio"></textarea>
            <br>
        </form>
        <a href="welcome">
            <button>Sign Up</button>
        </a>
    </body>
</html>